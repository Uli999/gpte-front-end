import { extendTheme } from "@chakra-ui/react";
import { graphqlSync } from "graphql";

const theme = extendTheme({
    colors: {
      brand: {
        100: "#f7fafc",
        900: "#1a202c",
      },
    },
    fonts: {
      heading: "Roboto Condensed",
      body: "Inter",
    },
    components: {
      Heading: {
        baseStyle: {
          color: "purple.100",
        },
        variants: {
          "page-heading": {
            color: "red.900",
            py: "4",
          },
        },
      },
      Link: {
        baseStyle: {
          color: "orange.200",
        },
      },
      }
    });
  export default theme;