import { ApolloClient, InMemoryCache } from "@apollo/client";

// Choose a network:
const client = new ApolloClient({
    // Mainnet:
    // uri: "https://d.graphql-api.mainnet.dandelion.link/",
    // Preproduction Testnet
    uri: "https://graphql-api.iohk-preprod.dandelion.link/",
    cache: new InMemoryCache(),
});

export default client;