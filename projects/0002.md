---
id: "0002"
datePosted: "2022-11-02"
title: "Create a style for commitToProject"
lovelace: 20000000
gimbals: 400
status: "Open"
devCategory: "Front End"
bbk: [""]
approvalProcess: 2
multipleCommitments: true
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end"
---

## Outcome:
- Create a unique Commitment Component in the [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) repo.

## Requirements:
- Your component must work when used in the route `/Projects/[id].tsx`
- The issuer of this Project will not pass judgement on the subjective beauty of your component. Try to make something that you're proud of, but don't worry if it's not award-winning design. The primary goal of this Project is to help you learn about GPTE.


## How To Start:
- Run [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) locally and navigate to [localhost:3000/projects/0001](http://localhost:3000/projects) to see the current user experience for pressing the "Commit to Project" button.
- Make a copy of [components/transactions/commitToProject.tsx](), and rename it "commitToProject___.tsx", replacing the blank space with some illustrative name like "commitToProjectOrangeTheme.tsx".
- In your new file, search for "GPTE Project 0002", and change the code between the START and END lines to provide your take on UX/UI for the Project Commitment and confirmation.

## Links + Tips:
- This component includes a lot of lines of code, but follows a common React design pattern: along with a button that users can press, we include the logic of what should happen when they do. You can delete the Box component (after "END OF GPTE Project 0002") that is provided for learning purposes to make your component more portable. Everything above the final return statement is necessary for building a successful transaction.
- You can choose what subset of available data to use in your component. Decide what you think is essential, and add/remove Project fields accordingly. You can review [project Types](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end/-/blob/main/types/index.ts) to see what data is available.
- Imagine that GPTE ships with a variety of Project Commitment components out of the box, that users can select from when they build their GPTE instance. This task is an initial step toward building something like [Wordpress Themes](https://wordpress.com/themes), that provide different styling and UI on top of the same underlying data.
- To get a feel for how community components work in a React/NextJS project, review Mastery Assignments [302.3](https://gimbalabs.instructure.com/courses/26/assignments/467) and [302.4](https://gimbalabs.instructure.com/courses/26/assignments/468) in Canvas.
- Review the [ChakraUI documentation](https://chakra-ui.com/docs/styled-system/style-props) to learn more about styling components.

## How To Complete:
- Submit a Merge Request to [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) with your new component in the [components/transaction directory](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end/-/tree/main/components/transaction)
- Note: We need to teach people how to use Git and GitLab: when to fork and when to clone.
- How does negotiation work with Project parameters?