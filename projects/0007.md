---
id: "0007"
datePosted: "2022-09-29"
title: "Create an Interface for Issuer to Manage the Treasury"
lovelace: 200000000
gimbals: 4000
status: "Open"
devCategory: "Front End"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end"
---

## Outcome:
- Build a front-end interface for Treasury Issuers to initialize a new Treasury by locking tokens.

## Requirements:
- Create a new page at localhost:3000/treasury
- For the Issuer, this page should display some Treasury Management features: including the ability to initialize or update a new Treasury.
- The Issuer should also be able to update the inline datum at the Treasury Contract address with a new list of Project Hashes.
- For all other visitors, this page should only display Treasury Status.
- [See prior version for an example]() and the [source code]()

## How To Start:
- To test your work on this Project, you will need to create your own instance of GPTE, with your own instances of Treasury and Escrow Contracts.
- Your instance will need to use PubKeyHash of an Issuer using a Browser Wallet like Nami or Eternl. You can get the PubKeyHash of a an address like this: [https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-front-end-template/-/blob/main/pages/utilities/working-with-pubkeyhash/index.tsx](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-front-end-template/-/blob/main/pages/utilities/working-with-pubkeyhash/index.tsx).
- When you have an instance of GPTE running, you'll be able to test your new component.

## Links + Tips:
- Look at [the issuer-funds-treasury script in GPTE-plutus](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-plutus/-/blob/master/scripts/01-issuer-funds-treasury.sh). Your task is to recreate this transaction in the web browser, using a Browser Wallet.
- Once a Treasury Contract has tokens, this task is complete - so you really only get one chance to test it...
- ...unless you have a way to *empty* a Treasury Contract. Our [TreasuryValidator](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-plutus/-/blob/master/src/GPTE/TreasuryValidator.hs) provides a "back door" for Issuers. Can you create a way to use this?

## How To Complete:
- Do not (yet) worry about the case where there is already a UTXO in the Treasury. That's another Project (0019).
- Submit a Merge Request to [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) with your new `/treasury` Page and any accompanying Components.