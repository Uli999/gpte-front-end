---
id: "0012"
datePosted: "2022-09-29"
title: "Compare TreasuryAction with BountyEscrowDatum"
lovelace: 150000000
gimbals: 3000
status: "Complete"
devCategory: "Plutus"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-plutus-v2"
---

## Outcome:
- The TreasuryValidator should validate only if all Project Parameters in TreasuryAction and BountyEscrowDatum match.

## Requirements:
- Implement Validation Logic in `/gbte-plutus-v2/src/GBTE/TreasuryValidator.hs` so that a Commitment transaction only validates when the Project Parameters in `Commit BountyDetails` and `BountyEscrowDatum` are the same.

## How To Start:
- Pull the latest changes from [GBTE PlutusV2](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus-v2)
- Create a new branch

## Links + Tips:
Review these examples:



## How To Complete:
- Present your solution at Live Coding
- Push new Branch to [GBTE PlutusV2](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gbte/gbte-plutus-v2) with changes.
- These changes will create a new version of the GPTE Plutus Contract. We will work as a team to decide when it is time to redeploy the Contracts.
