import { Box, Heading, Grid, GridItem } from "@chakra-ui/react";
import { NextPage } from "next";
import Image from "next/image";

import TreasurySummary from "../components/treasury/TreasurySummary";
import AboutInstance from "../components/about-instance/AboutInstance";

const Home: NextPage = () => {
  return (
    <Box>
      <Heading size="4xl" textAlign="center" py="5">
        Gimbal Project Treasury + Escrow Dapp
      </Heading>
      <Grid gridTemplateColumns="repeat(2, 1fr)" gap="5">
        <GridItem>
          <AboutInstance />
        </GridItem>
        <GridItem>
          <TreasurySummary />
        </GridItem>
        <GridItem colSpan={2} pt="10">
          <Box
            w="50%"
            mx="auto"
            p="5"
            border="1px"
            borderRadius="lg"
            boxShadow="2xl"
          >
            <Image
              src="/gpteDiagram.png"
              width="1200"
              height="675"
              alt="gbte-visualization"
            />
          </Box>
        </GridItem>
      </Grid>
    </Box>
  );
};

export default Home;
