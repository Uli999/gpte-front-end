import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import {
  Box,
  Heading,
  Center,
  Spinner,
  Text,
  Button,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Grid,
  GridItem,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
} from "@chakra-ui/react";
import {
  GraphQLInputUTxO,
  GraphQLToken,
  GraphQLUTxO,
  ProjectTxMetadata,
} from "../../types";
import { Asset, Transaction, UTxO, KoiosProvider, Data } from "@meshsdk/core";
import { useWallet, useAddress } from "@meshsdk/react";
import { treasury } from "../../cardano/plutus/treasuryContract";
import { hexToString } from "../../cardano/utils";
import {
  escrow,
  escrowReferenceUTxO,
} from "../../cardano/plutus/escrowContract";
import {
  referenceTokenReferenceUTxO,
  referenceValidatorAddress,
} from "../../cardano/plutus/referenceTokenContract";
import { getInlineDatumForContributorReference } from "../../cardano/gpte-providers/getInlineDatumForContributorReference";

// To Do
// [ ] Visually enable Distribution if Issuer Token is present

const ESCROW_QUERY = gql`
  query CommitmentTransactionDetails($transactionHash: Hash32Hex!) {
    transactions(where: { hash: { _eq: $transactionHash } }) {
      inputs {
        address
        value
        tokens {
          asset {
            policyId
            assetName
          }
          quantity
        }
      }
      outputs {
        txHash
        index
        address
        value
        tokens {
          asset {
            policyId
            assetName
          }
          quantity
        }
        datum {
          bytes
        }
      }
      metadata {
        key
        value
      }
    }
  }
`;

function getContributorTokenAssetId(tokens: GraphQLToken[], policyId: string) {
  const contribToken = tokens.filter(
    (t: GraphQLToken) => t.asset.policyId == policyId
  );

  const contributorTokenHex = contribToken[0].asset.assetName;
  const contributorTokenAssetId = policyId + contributorTokenHex;
  return contributorTokenAssetId;
}

type Props = {
  txHash: string;
};

const EscrowTransactionDetails: React.FC<Props> = ({ txHash }) => {
  const { connected, wallet } = useWallet();
  const connectedAddress = useAddress();
  const [txLoading, setTxLoading] = useState(false);
  const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null);
  const [showDevStuff, setShowDevStuff] = useState(false);

  const metadataKeyInt = parseInt(treasury.metadataKey);
  const contributorPolicyId = treasury.accessTokenPolicyId;
  const [connectedIssuerTokenName, setConnectedIssuerTokenName] = useState<
    string | null
  >(null);
  const [connectedIssuerAsset, setConnectedIssuerAsset] =
    useState<Asset | null>(null);

  const [contributorAssetId, setContributorAssetId] = useState<string>("");
  const [contributorAddress, setContributorAddress] = useState<string>("");
  const [contributorTokenName, setContributorTokenName] = useState<string>("");
  const [contribInput, setContribInput] = useState<GraphQLInputUTxO | null>(
    null
  );

  const [contributorReferenceUTxO, setContributorReferenceUTxO] =
    useState<UTxO | null>(null);

  const [updatedContributorReferenceUTxO, setUpdatedContributorReferenceUTxO] =
    useState<Partial<UTxO> | null>(null);

  const [contributorReferenceDatum, setContributorReferenceDatum] =
    useState<any>(null);

  const [
    updatedContributorReferenceDatum,
    setUpdatedContributorReferenceDatum,
  ] = useState<Data | null>(null);

  // For Chakra Modal:
  const { isOpen, onOpen, onClose } = useDisclosure();

  let gimbalDivisor = 1;
  const koiosProvider = new KoiosProvider("preprod");
  if (treasury.network == "1") {
    gimbalDivisor = 1000000;
  }

  // ------------------------------------------------------------------
  // The Issuer Token Asset: required by Escrow Contract
  // ------------------------------------------------------------------
  useEffect(() => {
    const fetchIssuerToken = async () => {
      const _token = await wallet.getPolicyIdAssets(
        treasury.issuerTokenPolicyId
      );
      if (_token.length > 0) {
        setConnectedIssuerTokenName(_token[0].assetName);
        setConnectedIssuerAsset({
          unit: _token[0].unit,
          quantity: "1",
        });
      }
    };
    if (connected) {
      fetchIssuerToken();
    }
  }, [connected]);

  const { data, loading, error } = useQuery(ESCROW_QUERY, {
    variables: {
      transactionHash: txHash,
    },
  });

  useEffect(() => {
    // The query is for "transactions with a certain txHash", and only one Tx will ever have a given hash.
    // That's why it's ok to hard-code the array index 0 here:
    if (data) {
      const _contribInput: GraphQLInputUTxO[] =
        data.transactions[0].inputs.filter((i: GraphQLInputUTxO) =>
          i.tokens.some(
            (t: GraphQLToken) => t.asset.policyId == contributorPolicyId
          )
        );

      const _contributorAddress = _contribInput[0].address;

      const _contributorTokenAssetId = getContributorTokenAssetId(
        _contribInput[0].tokens,
        contributorPolicyId
      );
      const _contributorTokenName = hexToString(
        _contributorTokenAssetId.substring(56)
      );

      setContribInput(_contribInput[0]);
      setContributorAssetId(_contributorTokenAssetId);
      setContributorAddress(_contributorAddress);
      setContributorTokenName(_contributorTokenName);
    }
  }, [data]);

  // This gets the UTxO at the Reference Validator Address, but it doesn't give us inline datum...
  useEffect(() => {
    const fetchContributorReferenceUTxO = async () => {
      const referenceAssetId =
        contributorAssetId.substring(0, 56) +
        "313030" +
        contributorAssetId.substring(62);
      const _refUTxO = await koiosProvider.fetchAddressUTxOs(
        referenceValidatorAddress,
        referenceAssetId
      );

      const _datum = await getInlineDatumForContributorReference(
        referenceAssetId
      );

      const masteryMap: Data = new Map<Data, Data>();
      _datum.fields[0].map.map(
        (mastery: { k: { bytes: string }; v: { int: number } }) => {
          const desc: Data = hexToString(mastery.k.bytes);
          const val: Data = mastery.v.int;
          masteryMap.set(desc, val);
        }
      );

      // Increment contribution count for Updated Inline Datum
      const contribCount = _datum.fields[1].int + 1;
      const alias = hexToString(_datum.fields[2].bytes);

      const _updatedDatum: Data = {
        alternative: 0,
        fields: [masteryMap, contribCount, alias],
      };

      const _updatedContributorReferenceUTxO: Partial<UTxO> = {
        output: {
          address: referenceValidatorAddress,
          amount: _refUTxO[0].output.amount,
        },
      };

      if (_refUTxO) {
        setContributorReferenceUTxO(_refUTxO[0]);
        setContributorReferenceDatum(_datum);
        setUpdatedContributorReferenceDatum(_updatedDatum);
        setUpdatedContributorReferenceUTxO(_updatedContributorReferenceUTxO);
      }
    };
    if (contributorAssetId) {
      fetchContributorReferenceUTxO();
    }
  }, [contributorAssetId]);

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  // Create a metadata object for Distribution Tx, from Commitment Tx returned by query
  const _metadata: ProjectTxMetadata = {
    id: data.transactions[0].metadata[0].value.id,
    hash: data.transactions[0].metadata[0].value.hash,
    expTime: data.transactions[0].metadata[0].value.expTime,
    txType: "Distribute",
    contributor: data.transactions[0].metadata[0].value.contributor,
  };

  // ------------------------------------------------------------------
  // Get Contributor Details: from input UTxO to Commitment Tx
  // ------------------------------------------------------------------

  // ------------------------------------------------------------------
  // Get Commitment Details: from Escrow UTxO
  // ------------------------------------------------------------------

  const _escrowOutput = data.transactions[0].outputs.filter(
    (i: GraphQLUTxO) => i.address == escrow.address
  );

  const _escrowUTxOIndex = _escrowOutput[0].index;

  const _commitmentInlineDatum = _escrowOutput[0].datum.bytes;

  const _lovelaceInCommitment = _escrowOutput[0].value;

  const _gimbalToken = _escrowOutput[0].tokens.filter(
    (t: GraphQLToken) => t.asset.policyId == treasury.projectTokenPolicyId
  );

  const _gimbalsInCommitment = _gimbalToken[0].quantity;

  // ------------------------------------------------------------------
  // Construct the UTxO to send to Contributor
  // ------------------------------------------------------------------

  const lovelaceToContributor: Asset = {
    unit: "lovelace",
    quantity: _lovelaceInCommitment,
  };

  const gimbalsToContributor: Asset = {
    unit: treasury.projectTokenAssetId,
    quantity: _gimbalsInCommitment,
  };

  const contributorToken: Asset = {
    unit: contributorAssetId,
    quantity: "1",
  };

  const distributeUTxO: Partial<UTxO> = {
    output: {
      address: contributorAddress,
      amount: [lovelaceToContributor, gimbalsToContributor, contributorToken],
    },
  };

  // ------------------------------------------------------------------
  // Construct the Escrow UTxO that we're unlocking:
  // ------------------------------------------------------------------
  const _escrowContractUTxO: UTxO = {
    input: {
      txHash: txHash,
      outputIndex: _escrowUTxOIndex,
    },
    output: {
      address: escrow.address,
      amount: [lovelaceToContributor, gimbalsToContributor, contributorToken],
      plutusData: _commitmentInlineDatum,
    },
  };

  // ------------------------------------------------------------------
  // Redeemer: 1 = "Distribute"
  // ------------------------------------------------------------------
  const distributeAction = {
    data: { alternative: 1, fields: [] },
  };

  // ------------------------------------------------------------------
  // Build, Sign and Submit the Distribute Transaction
  // ------------------------------------------------------------------
  const handleDistributeTx = async () => {
    if (connected) {
      setTxLoading(true);
      const network = await wallet.getNetworkId();
      if (network == 1) {
        alert("Please connect to Cardano Preprod Testnet");
      } else {
        const tx = new Transaction({ initiator: wallet })
          .redeemValue({
            value: _escrowContractUTxO,
            script: escrowReferenceUTxO,
            datum: _escrowContractUTxO,
            redeemer: distributeAction,
          })
          .sendValue(contributorAddress, distributeUTxO)
          .sendAssets(connectedAddress, [connectedIssuerAsset])
          .setMetadata(metadataKeyInt, _metadata);
        console.log("So far so good.", tx);
        try {
          const unsignedTx = await tx.build();
          const signedTx = await wallet.signTx(unsignedTx, true);
          const _txHash = await wallet.submitTx(signedTx);
          setTxLoading(false);
          setSuccessfulTxHash(_txHash);
          onOpen();
        } catch (error: any) {
          if (error.info) {
            alert(error.info);
          } else {
            console.log(error);
          }
        }
      }
    }
  };

  // ------------------------------------------------------------------
  // Redeemer: 0 = "UpdateDatum" in Reference Validator
  // ------------------------------------------------------------------
  const updateDatumAction = {
    data: { alternative: 0, fields: [] },
  };

  // ------------------------------------------------------------------
  // Use this version of the transaction to increment Reference Datum
  // ------------------------------------------------------------------
  const handleDistributeAndUpdateReferenceDatumTx = async () => {
    if (connected) {
      setTxLoading(true);
      const network = await wallet.getNetworkId();
      if (network == 1) {
        alert("Please connect to Cardano Preprod Testnet");
      } else {
        const tx = new Transaction({ initiator: wallet })
          .redeemValue({
            value: _escrowContractUTxO,
            script: escrowReferenceUTxO,
            datum: _escrowContractUTxO,
            redeemer: distributeAction,
          })
          .redeemValue({
            value: contributorReferenceUTxO,
            script: referenceTokenReferenceUTxO,
            datum: contributorReferenceUTxO,
            redeemer: updateDatumAction,
          })
          .sendValue(contributorAddress, distributeUTxO)
          .sendValue(
            {
              address: referenceValidatorAddress,
              datum: {
                value: updatedContributorReferenceDatum,
                inline: true,
              },
            },
            updatedContributorReferenceUTxO
          )
          .sendAssets(connectedAddress, [connectedIssuerAsset])
          .setMetadata(metadataKeyInt, _metadata);
        console.log("So far so good.", tx);
        try {
          const unsignedTx = await tx.build();
          const signedTx = await wallet.signTx(unsignedTx, true);
          const _txHash = await wallet.submitTx(signedTx);
          setTxLoading(false);
          setSuccessfulTxHash(_txHash);
          onOpen();
        } catch (error: any) {
          if (error.info) {
            alert(error.info);
          } else {
            console.log(error);
          }
        }
      }
    }
  };

  // ------------------------------------------------------------------
  // This version of GPTE has a "Dev Stuff" section
  // so that people can poke around
  // ------------------------------------------------------------------
  const toggleShowDevStuff = () => {
    setShowDevStuff(!showDevStuff);
  };

  return (
    <>
      <Box m="5" p="5" border="1px" borderRadius="lg">
        {connectedIssuerTokenName && (
          <Text>
            Issuer Token: {connectedIssuerTokenName} - better to pass as prop
            from SelectAndDistribute... component
          </Text>
        )}
        <Heading size="lg">
          Distribute Commitment to Project #{_metadata.id}
        </Heading>
        <Text>Contributor Asset Id: {contributorAssetId}</Text>
        <Text py="2">Project Hash: {_metadata.hash}</Text>
        <Text pb="2">Contributor Address: {contributorAddress}</Text>
        <Grid templateColumns="repeat(3, 1fr)" gap={6}>
          <Box bg="purple.100" color="blue.900" p="3">
            Contributor Token Name: {contributorTokenName}
          </Box>
          <Box bg="purple.100" color="blue.900" p="3">
            Ada in Commitment: {_lovelaceInCommitment / 1000000}
          </Box>
          <Box bg="purple.100" color="blue.900" p="3">
            Gimbals in Commitment: {_gimbalsInCommitment / gimbalDivisor}
          </Box>
        </Grid>
        <Text py="2">Commitment TxHash: {txHash}</Text>

        <Grid templateColumns="repeat(2, 1fr)" gap={6}>
          <GridItem>
            <Button
              onClick={handleDistributeTx}
              colorScheme="green"
              mx="5"
              my="2"
            >
              Distribute this Commitment
            </Button>
            <Button
              onClick={handleDistributeAndUpdateReferenceDatumTx}
              colorScheme="orange"
              mx="5"
              my="2"
            >
              Distribute and Update Contrib Datum
            </Button>
            <Button
              onClick={toggleShowDevStuff}
              mx="5"
              my="2"
              colorScheme="purple"
            >
              View Dev Details
            </Button>
          </GridItem>
          <GridItem>
            <Box p="2" bg="teal.900">
              <Text py="1">Status</Text>
              {txLoading ? (
                <Center>
                  <Spinner />
                </Center>
              ) : (
                <Text>
                  {successfulTxHash && `Successful Tx: ${successfulTxHash}`}
                </Text>
              )}
            </Box>
          </GridItem>
        </Grid>

        {/* "DevStuff - feel free to remove this section */}
        {showDevStuff && (
          <Accordion allowToggle>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    Metadata:
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  This metadata will be included on the Distribution
                  transaction. It is posted at metadata key {metadataKeyInt}. We
                  reconstruct this metadata by copying it from the Commitment
                  transaction, and changing from Commitment to Distribute.
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(_metadata, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    Input with Contributor Token:
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  This is what an input looks like in our Cardano GraphQL query.
                  It is the input from the Contributor to the Commitment
                  Transaction. Notice that in this data, we can find the
                  Contributor address and the assetName of the Contributor
                  token. Both are used in the Distribute transaction. This is an
                  example of how you can make clever use of the blockchain
                  history to get the transaction details you need.
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(contribInput, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    Escrow Output:
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  Similar to the Contributor Input shown above, this is what an
                  output looks like in our GraphQL query. It is the output to
                  the Escrow Contract from the Commitment Transaction. We can
                  use this data to see the amount of lovelace and gimbals in the
                  Commitment. There is also inline datum in this UTxO - which
                  makes it much easier to unlock a UTxO from a contract.
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(_escrowOutput, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    UTxO for Contributor
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  Here is what gets sent to the Contributor in this transaction.
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(distributeUTxO, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    UTxO We Want to Unlock
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  Putting it all together, this is how a UTxO is represented in
                  MeshSDK. This is the UTxO that is currently locked at the
                  Escrow Contract Address. We will unlock it with this
                  transaction.
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(_escrowContractUTxO, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    Contributor Reference UTxO
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  At another Contract Address, we store Contributor Reference.
                  In the Distribute Transaction, this UTxO is unlocked with
                  .redeemValue UTxOs.
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(contributorReferenceUTxO, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    Contributor Reference Datum
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  Here is the Datum returned by
                  getInlineDatumForContributorReference
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(contributorReferenceDatum, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
            <AccordionItem>
              <AccordionButton color="yellow.100">
                <Box flex="1" textAlign="left">
                  <Heading size="md" fontWeight="200" color="yellow.100">
                    More Stuff:
                  </Heading>
                </Box>
                <AccordionIcon />
              </AccordionButton>
              <AccordionPanel pb="4" fontSize="xs">
                <Text fontSize="md">
                  Here is the Datum returned by
                  getInlineDatumForContributorReference
                </Text>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(updatedContributorReferenceDatum, null, 2)}
                  </code>
                </pre>
                <pre>
                  <code className="language-js">
                    {JSON.stringify(updatedContributorReferenceUTxO, null, 2)}
                  </code>
                </pre>
              </AccordionPanel>
            </AccordionItem>
          </Accordion>
        )}
        {/* End of Dev Stuff Section */}
      </Box>
      <Modal blockScrollOnMount={false} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Successful Distribute Transaction</ModalHeader>
          <ModalBody>
            <Text py="2">Transaction ID: {successfulTxHash}</Text>
            <Text py="2">
              It may take a few minutes for this tx to show up on a blockchain
              explorer.
            </Text>
          </ModalBody>
          <ModalFooter>
            <Button onClick={onClose}>Close</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default EscrowTransactionDetails;
