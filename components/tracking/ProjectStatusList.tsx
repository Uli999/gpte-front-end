import Link from "next/link";
import { Box, Heading, Text, Link as ChakraLink } from "@chakra-ui/react";
import { ProjectStatus, EscrowTx } from "../../types";
import { getAllProjectStatus } from "../../project-lib/utils";

type Props = {
  transactions: EscrowTx[];
};

const ProjectStatusList: React.FC<Props> = ({ transactions }) => {
  const allEscrow: ProjectStatus[] = getAllProjectStatus(transactions);

  return (
    <Box>
      <Heading py="2">List of Status by Project:</Heading>
      <Text py="1" fontSize="xl">
        How can we use this data?
      </Text>
      <Text py="1" fontSize="xl">
        Try it:{" "}
        <Link href="/projects/0015">
          <ChakraLink>Project 0015</ChakraLink>
        </Link>
      </Text>
      {allEscrow.map((p: ProjectStatus) => (
        <Box key={p.id} m="5" p="5" bg="white" color="black">
          <Text fontSize="xl" fontWeight="900">
            <Link href={`/projects/${p.id}`}>
              <ChakraLink color="red.900">Project: {p.id}</ChakraLink>
            </Link>
          </Text>
          <Text>Project Hash: {p.hash[0]}</Text>
          {p.transactions.map((tx: EscrowTx, index) => (
            <Box key={index} py="1" fontSize="sm">
              <Text>
                {tx.type}: {tx.outputs[0].input.txHash}
              </Text>
              <Text>ContributorPkh: {tx.metadata.contributor}</Text>
              <Text>Contributor Token Name: {tx.contributorTokenName}</Text>
              <Text>Expiration time: {tx.metadata.expTime}</Text>
            </Box>
          ))}
        </Box>
      ))}
    </Box>
  );
};

export default ProjectStatusList;
