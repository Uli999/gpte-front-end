import { EscrowTx } from "./project"

export declare type ContributorReputation = {
    alias: string,
    completedCommitments: number,
    masteryData: MasteryDatum[],
    instructor: boolean
}

export declare type MasteryDatum = {
    description: string,
    value: number
}

export declare type ContributorHistory = {
    contributorTokenName: string,
    transactions: EscrowTx[]
}