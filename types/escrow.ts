import { ProjectTxMetadata } from "./project"
import { GraphQLInputUTxO, GraphQLToken, GraphQLUTxO } from "./gqlResult"

export declare type escrowTxFromMetadataKey = {
    hash: string,
    includedAt: string,
    metadata: [{
        "key": "161803", // Test: What error will we get when another metadata key is included in tx?
        "value": ProjectTxMetadata
    }],
    inputs: GraphQLInputUTxO[]
    outputs: GraphQLUTxO[]
    index: number,
    value: string,
    tokens: GraphQLToken[]
}