# GPTE Front End

## Current Branches:
- `main` for current development; configured for preprod
- `current-preprod` currently follows main, with Treasury Address: `addr_test1wpr838k666akr3p5k8tfcdfenrlzpueq2j87tp7zkx6mh8qm8maf8`
- `mainnet-instance-001` live instance, with Treasury Address: `addr1wxsktamev7v89wgv74xwl52wygrstpcjjqexkr42kjnpucqkxpp4a`


1. `main` uses Treasury Address  on Pre-Production and uses an implementation of [GPTE Plutus](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-plutus)

## To run the project
```
git clone https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte/gpte-front-end
cd gpte-front-end
yarn
yarn dev
```
In a browser, go to https://localhost:3000 and connect a Wallet. This project can connect to Browser Wallets on Cardano Mainnet, Testnet, Pre-Production, and Preview. Currently, most of the transaction functionality is configured to work with Pre-Production, and has been tested with Eternl Wallet on Pre-Production. `Only a few changes are required to make this template work with other Cardano networks, like Mainnet and Preview.

## Docs
- https://nextjs.org/docs/getting-started
- https://meshjs.dev/
